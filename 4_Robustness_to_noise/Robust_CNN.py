#========================= This cell will initialize CNN with BAD weights ================================
from __future__ import division
#======================= Importing libraries and Data ===========================
#
# %matplotlib inline


import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.log_device_placement = True
config.allow_soft_placement = False
CUDA_VISIBLE_DEVICES = 1
sess = tf.InteractiveSession(config=config)

import numpy as np
from scipy.misc import imsave
from scipy.misc import imresize
from sklearn.cluster import KMeans
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
import seaborn
import math
import matplotlib.gridspec as gridspec
import os
import pylab
import time

from matplotlib import rcParams

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
########################################################################
#                    drawing progressbar in console
########################################################################

#import libraries
import progressbar as pb

#define progress timer class
class progress_timer:

    def __init__(self, n_iter, description="Something"):
        self.n_iter         = n_iter
        self.iter           = 0
        self.description    = description + ': '
        self.timer          = None
        self.initialize()

    def initialize(self):
        #initialize timer
        widgets = [self.description, pb.Percentage(), ' ',   
                   pb.Bar(marker=pb.RotatingMarker()), ' ', pb.ETA()]
        self.timer = pb.ProgressBar(widgets=widgets, maxval=self.n_iter).start()

    def update(self, q=1):
        #update timer
        self.timer.update(self.iter)
        self.iter += q

    def finish(self):
        #end timer
        self.timer.finish()
        
# #initialize
# pt = progress_timer(description= 'For loop example', n_iter=10000000)  
# #for loop example
# for i in range(0,10000000):  
#     #update
#     pt.update()
# #finish
# pt.finish()  
     
        
    

########################################################################
#                    Constructing adversary data for research
########################################################################

def label_corrupter(y_train, noise):
    y_train_corrupted = np.array(y_train)
    corr_lev = int(noise*len(y_train))
    np.random.shuffle(y_train_corrupted[:corr_lev])
    
    return y_train_corrupted
#======================= Model definition AND INITIALIZATION ===========================
H = [512]
batch_s = [32]
for h in H:
    for bat in batch_s:
        #======================= Model definition ===========================
        x = tf.placeholder(tf.float32, shape=[None, 784])
        y_ = tf.placeholder(tf.float32, shape=[None, 10])

        def weight_variable(shape):
          initial = tf.truncated_normal(shape, stddev=0.1)
          return tf.Variable(initial)

        def bias_variable(shape):
          initial = tf.constant(0.1, shape=shape)
          return tf.Variable(initial)

        def conv2d(x, W):
          return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

        def max_pool_2x2(x):
          return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                                strides=[1, 2, 2, 1], padding='SAME')

        W_conv1 = weight_variable([5, 5, 1, 16])
        b_conv1 = bias_variable([16])

        x_image = tf.reshape(x, [-1,28,28,1])

        h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
        h_pool1 = max_pool_2x2(h_conv1)

        W_conv2 = weight_variable([5, 5, 16, 32])
        b_conv2 = bias_variable([32])

        h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
        h_pool2 = max_pool_2x2(h_conv2)

        W_fc1 = weight_variable([7 * 7 * 32, h])
        b_fc1 = bias_variable([h])

        h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*32])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

        keep_prob = tf.placeholder(tf.float32)
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

        W_fc2 = weight_variable([h, 10])
        b_fc2 = bias_variable([10])

        y = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

        #================================= Training ========================================
        cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
        train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
        correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        Train_accuracy = []
        Train_loss = []
        Test_accuracy = []
        Test_loss = []
        N_iter_max = 180000
        corruption = np.linspace(0,1, 15)
        for noise in corruption:
            N_iter = 20000 + int(noise*N_iter_max)
            sess.run(tf.global_variables_initializer())
            #=================== Data Preparation ================================
            X_train, y_train = mnist.train.images, label_corrupter(mnist.train.labels, noise)
            X_test, y_test = mnist.test.images, mnist.test.labels
            # X_validation, y_validation = 

            train_size = len(y_train)
            test_size = len(y_test)
            # validation_size = len(y_validation)

            class Dataset(object):
                class Set_part(object):
                    def __init__(self, data_type, set_type):
                        self.set_type = set_type
                        self.epoch = 0
                        self.i = 0
                        if set_type == 'train':
                            self.size = train_size
            #             elif set_type == 'validation':
            #                 self.size = validation_size
                        elif set_type == 'test':
                            self.size = test_size
                        else:
                            raise Exception('set types: train, test')

                        if data_type == 'usual':
                            exec('self.data = X_%s'%set_type)
                            exec('self.labels = y_%s'%set_type)

                        else:
                            raise Exception('data types: usual') 

                        self.idx = np.arange(self.size)
                        np.random.seed(42)
                        np.random.shuffle(self.idx)

                    def next_batch(self, batch_size):
                        rng = self.idx[self.i * batch_size: (self.i + 1) * batch_size]
                        batch = self.data.take(rng, 0)
                        batch_labels = self.labels.take(rng, 0)
                        self.i += 1
                        if self.i >= self.size / batch_size:
                            self.epoch += 1
                            self.i = 0
                            np.random.shuffle(self.idx)
                        return batch, batch_labels

                def __init__(self, data_type = 'usual'):
                    self.train = self.Set_part(data_type, 'train') 
            #         self.validation = self.Set_part(data_type, 'validation')
                    self.test = self.Set_part(data_type, 'test')

            d = Dataset()

            

            
            pt = progress_timer(description= 'H %d; B %d; N %.2f'%(h, bat, noise), n_iter=N_iter)
            for i in range(N_iter):
                batch = d.train.next_batch(bat)
                
                if i%500 == 0:
                    train_accuracy = accuracy.eval(feed_dict={
                        x:batch[0], y_: batch[1], keep_prob: 1.0})
                    Train_accuracy.append(train_accuracy)
                    
                    train_loss = cross_entropy.eval(feed_dict={
                        x:batch[0], y_: batch[1], keep_prob: 1.0})
                    Train_loss.append(train_loss)
                    
                    test_accuracy = accuracy.eval(feed_dict={
                        x:mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})
                    Test_accuracy.append(test_accuracy)
                    
                    test_loss = cross_entropy.eval(feed_dict={
                        x:mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})
                    Test_loss.append(test_loss)
                
                train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
                pt.update()
            
            plt.figure(1)
            plt.plot(np.arange(len(Train_accuracy))/len(Train_accuracy), Train_accuracy, label = 'Train')
            plt.plot(np.arange(len(Test_accuracy))/len(Test_accuracy), Test_accuracy, label = 'Test')
            pylab.title('CNN SGD Robustness N %.2f; I %d'%(noise, N_iter), fontsize=16)
            pylab.ylabel("Accuracy", fontsize=16)
            pylab.xlabel("Iter x500", fontsize=16)
            plt.legend()
            plt.plot()
            plt.savefig('CNN%d_ROBUST_ACC_SGD %.2f.pdf'%(h, noise))
            plt.clf()

            plt.figure(2)
            plt.semilogy(np.arange(len(Train_loss))/len(Train_loss), Train_loss, label = 'Train')
            plt.semilogy(np.arange(len(Test_loss))/len(Test_loss), Test_loss, label = 'Test')
            pylab.title('CNN SGD Robustness N %.2f; I %d'%(noise, N_iter), fontsize=16)
            pylab.ylabel("Loss", fontsize=16)
            pylab.xlabel("Iter x500", fontsize=16)
            plt.legend()
            plt.plot()
            plt.savefig('CNN%d_ROBUST_LOSS_SGD %.2f.pdf'%(h, noise))
            plt.clf()

            pt.finish()